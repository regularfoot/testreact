import {createStore, applyMiddleware, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers'
import { redirect } from '../middlewares/redirect'
import logger from 'redux-logger'

export default function configureStore(initialState) {
    const store = compose(

        applyMiddleware(thunkMiddleware),
        applyMiddleware(logger),
        applyMiddleware(redirect)

    )(createStore)(rootReducer);

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers').rootReducer;
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}