import React, {Component} from 'react'
import PropTypes from 'prop-types';

export default class Logout extends Component {


    render() {
        let template


        template = <button className='btn' onClick={this.props.handleLogout}>Выйти</button>


        return <div className='User Logout'>
            {template}
        </div>
    }
}

Logout.propTypes = {
    name: PropTypes.string.isRequired
}
Logout.contextTypes = {
    router: PropTypes.object.isRequired
}