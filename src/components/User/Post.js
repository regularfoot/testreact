import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {browserHistory} from 'react-router'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Logout from '../../components/User/Logout'
import * as userLogout from '../../actions/UserLogout'

class Post extends Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(e) {
        e.preventDefault();
        const value = e.target.elements[0].value.toLowerCase()
        if (value.length) {
            VK.api("wall.post", {"message": value}, function (data) { // eslint-disable-line no-undef
                browserHistory.push(`/me/`);
            });
        } else {
            alert('введите сообщение');
        }
    }

    render() {
        let template;
        const {user} = this.props;
        const {handleLogout} = this.props.userLogout;

        VK.Auth.getLoginStatus(function (response) { // eslint-disable-line no-undef
            if (response.session == null) {
                browserHistory.push(`/`);
            }
        });

        if (localStorage.getItem('user') == null) {
            localStorage.setItem('user', JSON.stringify(user));
        }

        let userLS = JSON.parse(localStorage.getItem('user'));

        if (userLS) {
            template =
                <div className="user-block">
                    <div className="user-block_photo"><img src={userLS.image} alt={userLS.name}/></div>
                    <div className="user-block_name">{userLS.name}</div>
                    <div className="user-block_age">{userLS.age}</div>
                    <div className="user-block_id">@{userLS.domain}</div>
                    <Logout name="logout" handleLogout={handleLogout}/>

                </div>;
        } else {
            template =
                <div className="user-block">
                    Не удалось получить данные
                </div>;
        }
        return (
            <div className='container'>
                <div className="header">
                    <div className="container-inner">
                        {userLS.name}, мы рады видеть Вас
                    </div>
                </div>
                {template}
                <form className='send-post' onSubmit={this.handleSubmit}>
                    Введите текстовое сообщение для публикации
                    <textarea type='text'></textarea>
                    <button type='submit'>Опубликовать</button>
                </form>
            </div>
        )
    }
}

Post.contextTypes = {
    router: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        user: state.user,
        page: state.page
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userLogout: bindActionCreators(userLogout, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)