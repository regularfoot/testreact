import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import {Link} from 'react-router'

import Logout from '../../components/User/Logout'
import * as userLogout from '../../actions/UserLogout'

class UserInfo extends Component {

    render() {
        const {handleLogout} = this.props.userLogout;

        const {user} = this.props;
        let template;

        VK.Auth.getLoginStatus(function (response) { // eslint-disable-line no-undef
            if (response.session == null) {
                browserHistory.push(`/`);
            }
        });

        if (localStorage.getItem('user') == null || !JSON.parse(localStorage.getItem('user')).name.length) {
            localStorage.setItem('user', JSON.stringify(user));
        }

        let userLS = JSON.parse(localStorage.getItem('user'));

        if (userLS) {
            localStorage.setItem('userId', userLS.userId);
            template =
                <div className="container">
                    <div className="header">
                        <div className="container-inner">
                            {userLS.name}, мы рады видеть Вас
                        </div>
                    </div>
                    <div className="user-block">
                        <div className="user-block_photo"><img src={userLS.image} alt=""/></div>
                        <div className="user-block_name">{userLS.name}</div>
                        <div className="user-block_age">{userLS.age}</div>
                        <div className="user-block_id">@{userLS.domain}</div>
                        <Logout name="logout" handleLogout={handleLogout}/>
                        <div className="btn-wrapper">
                            <Link to="/friends/" className='btn btn-arrow'>Мои друзья</Link>
                            <Link to="/me/post/" className='btn btn-arrow'>Новая запись</Link>
                        </div>
                    </div>
                </div>;
        } else {
            template =
                <div className="user-block">
                    Не удалось получить данные
                </div>;
        }

        return (
            <div className='ib user'>
                {template}
            </div>
        )

    }
}

UserInfo.contextTypes = {
    router: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.user,
        page: state.page
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userLogout: bindActionCreators(userLogout, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo)
