import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router'

export default class NotFound extends Component {

    render() {
        let isLogin = false;
        VK.Auth.getLoginStatus(function(response) { // eslint-disable-line no-undef
            isLogin = response.session
        });
        if (!isLogin) {
            browserHistory.push(`/`);
        }
        return (
            <div>
                NotFound 404
            </div>
        )
    }
}
NotFound.contextTypes = {
    router: PropTypes.object.isRequired
}