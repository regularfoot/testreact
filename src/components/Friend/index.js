import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'

class Friend extends Component {
    render() {
        const {user} = this.props;
        const userUid = this.props.location.pathname.slice(9);


        if (localStorage.getItem('userFriend') == null) {
            localStorage.setItem('userFriend', JSON.stringify(user.friends));
        }

        let fiendsList = JSON.parse(localStorage.getItem('userFriend'));

        function calcAge(dateString) {
            var birthday = +new Date(dateString);
            return ~~((Date.now() - birthday) / (31557600000));
        }

        function declOfNum(number, titles) {
            let cases = [2, 0, 1, 1, 1, 2];
            return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        }


        let friend = [];

        for (let key in fiendsList){
            if (fiendsList[key].uid == userUid) {
                friend = fiendsList[key];
            }
        }

        console.log(friend.bdate != undefined)
        let age = '';
        if (friend.bdate != undefined) {
            age = friend.bdate.split('.');
            age = calcAge(age[1] + '.' + age[0] + '.' + age[2]);
            age = age + ' ' + declOfNum(age, ['год', 'года', 'лет']);
        }

        return (
            <div className="container">
                <div className="header">
                    <div className="container-inner">
                        <Link to="/friends/">К списку друзей</Link>
                    </div>
                </div>
                <div className="user-block">
                    <div className="user-block_photo"><img src={friend.photo_200} alt={friend.name}/></div>
                    <div className="user-block_name">{friend.first_name} {friend.last_name}</div>

                    <div className="user-block_age">{age}</div>

                    <div className="user-block_id">@{friend.domain}</div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Friend)