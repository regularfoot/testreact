import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {browserHistory} from 'react-router'
import {Link} from 'react-router'

class Friends extends Component {

    render() {
        const {user} = this.props;

        VK.Auth.getLoginStatus(function (response) { // eslint-disable-line no-undef
            console.log(response.session )
            if (response.session == null) {
                browserHistory.push(`/`);
            }
        });

        if (localStorage.getItem('userFriend') == null) {
            localStorage.setItem('userFriend', JSON.stringify(user.friends));
        }

        let fiendsList = JSON.parse(localStorage.getItem('userFriend'));

        return (
            <div className="container">
                <div className="header">
                    <div className="container-inner">
                        <Link to="/me/">Назад</Link>
                    </div>
                </div>
                <div className='friends-block-wrap'>
                    {fiendsList.map((entry, index) =>
                        <div key={index} className='friends-block'>
                            <Link to={/friends/ + entry.uid} className="item"></Link>
                            <div className="img"><img src={entry.photo_200} alt=""/></div>
                            <div className="first-name">{entry.first_name}</div>
                            <div className="last-name">{entry.last_name}</div>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

Friends.contextTypes = {
    router: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Friends)