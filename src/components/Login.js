import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import User from '../components/User'
import * as userActions from '../actions/UserActions'


class Login extends Component {

    render() {

        const {user} = this.props;
        const {handleLogin} = this.props.userActions;


        return (
            <div className="container">
                <div className="header">
                    <div className="container-inner">
                        Добро пожаловать!
                    </div>
                </div>
                <div className="login-form">
                    <div className="login-form_title">
                        Вход на сайт
                    </div>
                    <div className="login-form_subtitle">
                        Авторизуйтесь через социальную сеть Вконтакте.
                    </div>

                    <User name={user.name} handleLogin={handleLogin} error={user.error}/>
                </div>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

