import React, {Component} from 'react'
import PropTypes from 'prop-types';
// import {browserHistory} from 'react-router'

export default class User extends Component {


    render() {
        // const {name} = this.props
        let template


        template = <button className='btn' onClick={this.props.handleLogin}>Войти</button>


        return <div className='ib user'>
            {template}
            {/*{error ? <p className='error'> {error}. <br /> Попробуйте еще раз.</p> : ''}*/}
        </div>
    }
}

User.propTypes = {
    name: PropTypes.string.isRequired
}
User.contextTypes = {
    router: PropTypes.object.isRequired
}