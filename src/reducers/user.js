import {
    USER_DATA_SUCCES,
    FRIENDS_DATA_SUCCES,
    LOGIN_FAIL
} from '../constants/User'

const initialState = {
    name: '',
    domain: '',
    image: '',
    id: '',
    age: '',
    error: '',
    friends: '',
    userId: ''
}

export default function user(state = initialState, action) {

    switch (action.type) {
        case USER_DATA_SUCCES:
            return {...state,
                name: action.name,
                domain: action.domain,
                image: action.image,
                id: action.id,
                age: action.age,
                userId: action.userId,
                error: ''}

        case FRIENDS_DATA_SUCCES:
            return {...state,
                friends: action.friends,
                error: ''}


        case LOGIN_FAIL:
            return {...state, error: action.payload.message}

        default:
            return state
    }

}