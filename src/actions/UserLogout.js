import {
    ROUTING
} from '../constants/Routing'

export function handleLogout() {
    localStorage.removeItem('user');
    localStorage.removeItem('userFriend');

    return (dispatch) => {

        VK.Auth.logout((r) => { // eslint-disable-line no-undef
            dispatch({
                type: ROUTING,
                payload: {
                    method: 'replace',
                    nextUrl: '/'
                }
            })
        });



    }
}