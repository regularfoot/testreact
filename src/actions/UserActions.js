import {
    LOGIN_REQUEST,
    LOGIN_SUCCES,
    USER_DATA_SUCCES,
    FRIENDS_DATA_SUCCES,
    LOGIN_FAIL
} from '../constants/User'

import {
    ROUTING
} from '../constants/Routing'

export function handleLogin() {
    console.log('login')

    return (dispatch) => {

        dispatch({
            type: LOGIN_REQUEST
        })

        function calcAge(dateString) {
            var birthday = +new Date(dateString);
            return ~~((Date.now() - birthday) / (31557600000));
        }

        function declOfNum(number, titles) {
            let cases = [2, 0, 1, 1, 1, 2];
            return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        }


        VK.Auth.login((r) => { // eslint-disable-line no-undef
            if (r.session) {
                let username = r.session.user.first_name;
                let userId = r.session.user.id;


                dispatch({
                    type: LOGIN_SUCCES
                });
                VK.Api.call('users.get', {user_ids: userId, fields: 'domain, bdate, photo_200'}, (r) => { // eslint-disable-line no-undef
                    try {
                        if (r.response) {
                            let age = r.response[0].bdate.split('.');
                            age = calcAge(age[1] + '.' + age[0] + '.' + age[2]);
                            age = age + ' ' + declOfNum(age, ['год', 'года', 'лет']);

                            dispatch({
                                type: USER_DATA_SUCCES,
                                name: r.response[0].first_name + ' ' + r.response[0].last_name,
                                domain: r.response[0].domain,
                                image: r.response[0].photo_200,
                                age: age,
                                userId: userId,
                                payload: username,
                            });
                        }

                    }
                    catch (e) {

                    }
                });
                VK.Api.call('users.getFollowers', {user_ids: localStorage.getItem('userId'), count: 18, fields: 'domain, bdate, photo_200'}, (r) => { // eslint-disable-line no-undef
                    try {
                        if (r.response) {
                            dispatch({
                                type: FRIENDS_DATA_SUCCES,
                                friends: r.response.items
                            });
                        }
                    }
                    catch (e) {

                    }
                })


            } else {
                dispatch({
                    type: LOGIN_FAIL,
                    error: true,
                    payload: new Error('Ошибка авторизации')
                })
            }
            dispatch({
                type: ROUTING,
                payload: {
                    method: 'replace', //или, например, replace
                    nextUrl: '/me/'
                }
            })
        },2,4); // запрос прав на доступ к photo



    }

}