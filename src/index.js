import React from 'react';
import {render} from 'react-dom'
import {Provider} from 'react-redux'
// import './index.css'
import './styles/app.css'           // <-- импорт стилей
import App from './containers/App'
// import Admin from './components/Admin'
// import Genre from './components/Genre'
import Login from './components/Login'
// import List from './components/List'
// import Home from './components/Home'
import UserInfo from './components/User/index'
import NotFound from './components/NotFound'
import Friend from './components/Friend'
import Friends from './components/Friends'
import Post from './components/User/Post'
// import Release from './components/Release'

import {Router, Route, IndexRoute, browserHistory} from 'react-router'

import configureStore from './store/configureStore'

const store = configureStore();

render(
        <Provider store={store}>
            <Router history={browserHistory}>
                <Route path='/' component={App}>
                    <IndexRoute component={Login}/>

                    <Route path='/me/' component={UserInfo}/>
                    <Route path='/me/post' component={Post}/>
                    <Route path='/friends/:friend' component={Friend}/>
                    <Route path='/friends' component={Friends}>

                    </Route>
                    <Route path='*' component={NotFound} />
                </Route>
            </Router>
        </Provider>,
    document.getElementById('root')
);
/*
render(
    <Provider store={store}>
        <App />
    </Provider>,
    <Router history={browserHistory}>
        <Route path='/' component={App}>
        </Route>
    </Router>,
    document.getElementById('root')
);*/