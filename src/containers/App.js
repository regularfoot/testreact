import React, {Component} from 'react'
// import {Link} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import '../App.css';
// import User from '../components/User'
// import Page from '../components/Page'
import * as userActions from '../actions/UserActions'

// import Admin from '../components/Admin'
// import Genre from '../components/Genre'
// import Home from '../components/Home/index'
// import List from '../components/List'


// import AppHeader from './components/AppHeader';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            route: window.location.hash.substr(1)
        }
    }

    componentDidMount() {
        window.addEventListener('hashchange', () => {
            this.setState({
                route: window.location.hash.substr(1)
            })
        })
    }

    render() {
        // let Child;

        const {page} = this.props;
        //

        return (
            <div className="app">
                <div className='container-main'>
                    {/* добавили вывод потомков */}
                    {this.props.children}
                </div>

                <footer className="footer">
                    <div className="container-inner">
                        Подвал всегда внизу страницы
                        <span className="footer-date">{page.year}</span>
                    </div>
                </footer>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        page: state.page
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(userActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
